<?php

global $redis;
$redis = new Redis();
$redis->connect('127.0.0.1', 6380);
$redis->auth('quinton');

function getFirstLoadingPlugin()
{
    $files = scandir('plugin');
    foreach ($files as $dir) {
        if ($dir == "." || $dir == "..") {
            continue;
        }
        if (file_exists('plugin/' . $dir . '/index.php')) {
            $comment = getComment('plugin/' . $dir . '/index.php');
            if ($comment != null) {
                $lines = preg_split("/\r\n|\n|\r/", $comment);
                $r = array();
                foreach ($lines as $line) {
                    $temp = explode(":", $line);
                    if (count($temp) == 2) {
                        $r[$temp[0]] = $temp[1];
                    }
                }
                $r['path']=dirname(__FILE__).'/plugin/' . $dir . '/index.php';
                return $r;
            }
        }
    }
    return false;
}

function getComment($file)
{
    $tokens = token_get_all(file_get_contents($file));
    foreach ($tokens as $token) {
        // if (is_array($token)) {
        // 	echo "Line {$token[2]}: ", token_name($token[0]), " ('{$token[1]}')", "<br>";
        // }
        if (token_name($token[0]) == 'T_COMMENT') {
            return $token[1];
        }
    }
    return null;
}

function loadTheme()
{
    global $redis;
    $theme=$redis->get('theme');
    if ($theme=="") {
        $theme="peter";
    }
    include("theme/$theme/index.php");
}

function addHook($name, $func)
{
    $func();
}

function getHeader()
{
    echo "getHeader";
}

function getFooter()
{
    echo "getFooter";
}
